import subprocess
import os
import re
from pathlib import Path

info = """This script will perform some checks to identify release readiness (including readiness for Android APK build 
process for F-Droid)

Following checks will be performed:

1. Is the work tree a clean git repo with a valid version tag
2. Are the attribution files for the current version available
3. Does the versionName and versionCode in the AndroidManifest.xml match the APK 

This is helpful since VersionCodes cannot be defined in buildozer.spec and AndroidManifest.xml will only be used
in the F-Droid build process for signing the APK. Therefore the VersionCode and VersionName must match the APK.
This script can be run locally (with a local tag) before the tag is pushed to the source repo and an F-Droid build job
is triggered.

"""


def user_exit():
    print('User exit. Readiness NOT confirmed!')
    exit(0)


def remove_version_files():
    """Remove the temporary version files"""
    try:
        os.remove('simple_plotter4a/version.py')
    except FileNotFoundError:
        pass
    try:
        os.remove('simple_plotter4a/version.txt')
    except FileNotFoundError:
        pass
    print('Temporary version files removed.')


def shift_tag(version_str, files_to_add=None):
    """Deletes the current tag, commits changes and add tag again"""

    # delete tag
    subprocess.run(['git', 'tag', '-d', version_str])
    # commit changes - add new files and commit
    if files_to_add:
        for file in files_to_add:
            subprocess.run(['git', 'add', file])
    subprocess.run(['git', 'commit', '-m', 'Automatically created commit by check_release_readiness script'])
    # add tag again
    subprocess.run(['git', 'tag', '-a', version_str, '-m', 'Tag shifted to new commit'])
    print('Tag \'{}\' shifted to new commit'.format(version_str))


def split_version_code(version_str):
    """Splits a version string into integers for major, minor, patch"""
    match_code = re.match(pattern='^[0-9]+\.[0-9]+\.[0-9]+', string=version_str)
    if match_code:
        major = int(match_code.group().split('.')[0])
        minor = int(match_code.group().split('.')[1])
        patch = int(match_code.group().split('.')[2])
    else:
        raise ValueError('Could not extract version code!')
    return major, minor, patch


def check_attributions(version_str):
    """Checks if the attribution files for the corresponding verion string exist"""
    attr_file_android = 'attributions_android_{}.txt'.format(version_str)
    attr_file_python = 'attributions_python_{}.txt'.format(version_str)

    data_dir = Path('simple_plotter4a/data')
    lic_dir = Path('Licenses')

    print('\nChecking for attribution files for version {}...'.format(version_str))
    for root, dirs, files in os.walk(data_dir):
        if attr_file_android in files:
            attr_android = True
            print('Attribution file for Android found.')
        else:
            attr_android = False
            print('Warning: Attribution file for Android NOT found.')
        if attr_file_python in files:
            attr_python = True
            print('Attribution file for python found.')
        else:
            attr_python = False
            print('Warning: Attribution file for python NOT found.')

    if not attr_android or not attr_python:
        add_attr = input('Required attribution files missing - should the files be created now?[y/n=default]:')
        if add_attr in ['y', 'Y']:
            subprocess.run(['python', 'create_version_files.py'])
            subprocess.run(['python', 'Licenses/create_attributions.py'])
            remove_version_files()
            files_to_commit.append(Path.joinpath(data_dir, attr_file_android))
            files_to_commit.append(Path.joinpath(data_dir, attr_file_python))
            files_to_commit.append(Path.joinpath(lic_dir, 'attributions_{}.rst'.format(version_str)))
            print('Attribution files have been created.')
        else:
            user_exit()


def check_working_copy():
    """Checks if the local working copy is clean and tagged"""
    # check clean work dir
    print('\nChecking if work dir is clean...')
    output = subprocess.getoutput('git status --porcelain')
    if len(output) > 0:
        print('\nWork dir is not clean! Please clean the work dir first - see output of \'git status\'.')
        exit(1)
    else:
        print('Work tree clean - okay.')
    # check for tag
    print('\nChecking if current commit is tagged...')
    status, output = subprocess.getstatusoutput('git describe --exact-match --tags HEAD')
    if status == 0:
        current_tag = output
    else:
        output = subprocess.getoutput('git describe --abbrev=0 --tags')
        if len(output) > 0:
            major, minor, patch = split_version_code(output)
            patch += 1
            new_tag = '{}.{}.{}'.format(major, minor, patch)
        else:
            new_tag = '0.1.0'
        add_tag = input('Commit is not tagged '
                        '- shall the tag \'{}\' be assigned to the commit[y/n=default]:'.format(new_tag))
        if add_tag in ['y', 'Y']:
            subprocess.run(['git', 'tag', '-a', new_tag, '-m', 'tag added'])
            current_tag = new_tag
        else:
            current_tag = None
            user_exit()
    print('Current tag is:', current_tag)
    return current_tag


def get_manifest_data():
    """Extract the versionCode and versionName from AndroidManifest.xml"""
    with open('AndroidManifest.xml', 'r') as file:
        manifest_data = file.read()
        vercode_match = re.search('versionCode=\"[0-9]+\"', manifest_data)
        manifest_versionCode = vercode_match.group().split('\"')[1]
        vername_match = re.search('versionName=\"[0-9]+.[0-9]+.[0-9]+\"', manifest_data)
        manifest_versionName = vername_match.group().split('\"')[1]

    return manifest_versionCode, manifest_versionName, manifest_data


def get_apk_data():
    """Extracts the versionCode and versionName from an APK"""
    # get APK info - assuming APK is in bin-folder
    apk_file = None

    if not Path('bin').exists():
        os.mkdir('bin')
    else:
        for root, dirs, files in os.walk('bin'):
            for file in files:
                if '.apk' in file:
                    apk_file = file
                    print('Found APK:', apk_file)
                    break

    if apk_file is None:
        # raise FileNotFoundError('Could not find an APK file!')
        build_apk = input('Could not find an APK file - shall an APK be created?[y/n=default]:')
        if build_apk in ['y', 'Y']:
            print('\nStarting APK build...')
            subprocess.run(['ant', 'release'])
            for root, dirs, files in os.walk('bin'):
                for file in files:
                    if '.apk' in file:
                        apk_file = file
                        print('Found APK:', apk_file)
                        break
        else:
            user_exit()

    build_tools_path = get_android_build_tools_path()

    # run aapt to get APK infor
    status, output = subprocess.getstatusoutput('{}/aapt dump badging bin/{}'.format(build_tools_path, apk_file))

    # extract versionCode and versionName
    if status == 0:
        vercode_match = re.search('versionCode=\'[0-9]+\'', output)
        apk_versionCode = vercode_match.group().split('\'')[1]
        vername_match = re.search('versionName=\'[0-9]+.[0-9]+.[0-9]+\'', output)
        apk_versionName = vername_match.group().split('\'')[1]
        # print('APK versionCode: {}, versionName: {}'.format(apk_versionCode, apk_versionName))
    else:
        raise ValueError('aapt command failed...')

    return apk_versionCode, apk_versionName


def patch_manifest(version_code, version_name):
    """Patches the AndroidManifest.xml with a new versionCode and versionName"""
    with open('AndroidManifest.xml', 'r') as file:
        manifest_data = file.read()
    # replace version code and name
    manifest_data = re.sub('versionCode=\"[0-9]+\"', 'versionCode=\"{}\"'.format(version_code), manifest_data)
    manifest_data = re.sub('versionName=\"[0-9]+.[0-9]+.[0-9]+\"', 'versionName=\"{}\"'.format(version_name),
                           manifest_data)

    with open('AndroidManifest.xml', 'w') as file:
        file.write(manifest_data)
    print('\nAndroidManifest.xml has been updated. '
          'New versionCode=\"{}\", new versionName=\"{}\"'.format(version_code, version_name))


def get_android_build_tools_path():
    """Checks for the latest Android SDK build tools"""

    # try to find aapt in buildtools
    try:
        android_home = os.environ['ANDROID_HOME']
    except KeyError:
        raise KeyError('Could not find Android SDK folder. Please set ANDROID_HOME environment variable accordingly.')

    # try to get latest android build-tools
    build_tool_latest = None
    tool_code = 0
    for root, dirs, files in os.walk('{}/build-tools'.format(android_home)):
        for folder in dirs:
            match_code = re.match(pattern='^[0-9]+\.[0-9]+\.[0-9]+', string=folder)
            if match_code:
                major = int(match_code.group().split('.')[0])
                minor = int(match_code.group().split('.')[1])
                patch = int(match_code.group().split('.')[2])
                new_tool_code = major * 10**6 + minor * 10**3 + patch
                if new_tool_code > tool_code:
                    build_tool_latest = folder
                    tool_code = new_tool_code

    if build_tool_latest is None:
        raise FileNotFoundError('Could not find any version of Android SDK build-tools. Make sure build-tools is installed'
                                'in your ANDROID_HOME path.')

    # check if aapt is present
    aapt_found = os.path.isfile('{}/build-tools/{}/aapt'.format(android_home, build_tool_latest))
    if not aapt_found:
        raise FileNotFoundError('Could not find \'aapt\' in build-tools!')

    build_tools_path = Path(android_home + '/build-tools/' + build_tool_latest)

    print('Using Android SDK build-tools in:', build_tools_path)

    return build_tools_path


def check_android_manifest(version_str):
    """Will check the AndroidManifest.xml for valid information"""

    apk_versionCode, apk_versionName = get_apk_data()

    # first check, if APK belongs to current git tag
    if version_str != apk_versionName:
        print('No APK found for current release version {}.'.format(version_str))
        build_apk = input('Shall a new release APK be created? [y/n=default]:')
        if build_apk in ['y', 'Y']:
            print('\nStarting APK build...')
            subprocess.run(['ant', 'release'])
            apk_versionCode, apk_versionName = get_apk_data()
        else:
            user_exit()

    manifest_versionCode, manifest_versionName, manifest_data = get_manifest_data()

    # compare values
    print('Result comparision\n'
          '------------------')
    print('versionCodes - APK={} vs. AndroidManifest.xml={} - '.format(apk_versionCode, manifest_versionCode), end='')
    if apk_versionCode == manifest_versionCode:
        versionCodes_match = True
        print('match')
    else:
        versionCodes_match = False
        print('no match!')

    print('versionNames - APK={} vs. AndroidManifest.xml={} - '.format(apk_versionName, manifest_versionName), end='')
    if apk_versionName == manifest_versionName:
        versionNames_match = True
        print('match')
    else:
        versionNames_match = False
        print('no match!')

    if versionCodes_match and versionNames_match:
        print('versionCodes and versionNames okay - nothing to patch.\n')
        return apk_versionCode

    else:
        resp = input('Found mismatch between APK and AndroidManifest.xml - '
                     'shall AndroidManifest.xml be patched?[y/n=default]')
        if resp in ['y', 'Y']:
            patch_manifest(version_code=apk_versionCode, version_name=apk_versionName)
            files_to_commit.append('AndroidManifest.xml')
            return apk_versionCode
        else:
            user_exit()


def check_fastlane_changelog(versionCode):
    """Checks if the fastlane changelog file exists"""
    fastlane_changelogs_dir = 'fastlane/metadata/android/en-US/changelogs'
    if not Path(fastlane_changelogs_dir).exists():
        os.mkdir(fastlane_changelogs_dir)

    for root, dirs, files in os.walk(fastlane_changelogs_dir):
        if '{}.txt'.format(versionCode) in files:
            print('fastlane Changelog found.')
        else:
            create_log = input('No fastlane changelog found - shall it be created from CHANGELOG?[y/n=default]:')
            if create_log in ['y', 'Y']:
                status, output = subprocess.getstatusoutput('python update_fastlane_changelog.py')
                if status != 0:
                    print('Failed to update fastlane changelog - see output below:')
                    print(output)
                    exit(1)
                else:
                    print(output)
                    files_to_commit.append(Path(fastlane_changelogs_dir).joinpath('{}.txt'.format(versionCode)))
            else:
                user_exit()


if __name__ == '__main__':
    files_to_commit = []
    steps = 6
    print(info)

    current_step = 1
    print('\nStep {}/{}:'.format(current_step, steps))
    print('Checking if work dir is clean and tagged\n'
          '----------------------------------------')
    version = check_working_copy()
    print('\nStep {}/{}: Completed'.format(current_step, steps))
    current_step += 1

    print('\nStep {}/{}:'.format(current_step, steps))
    print('Checking if attribution files exist\n'
          '-----------------------------------')
    check_attributions(version_str=version)
    print('\nStep {}/{}: Completed'.format(current_step, steps))
    current_step += 1

    print('\nStep {}/{}:'.format(current_step, steps))
    print('Checking if AndroidManifest.xml is okay\n'
          '---------------------------------------')
    versionCode = check_android_manifest(version_str=version)
    print('\nStep {}/{}: Completed'.format(current_step, steps))
    current_step += 1

    print('\nStep {}/{}:'.format(current_step, steps))
    print('Checking if fastlane changelog file exists\n'
          '------------------------------------------')
    check_fastlane_changelog(versionCode)
    print('\nStep {}/{}: Completed'.format(current_step, steps))
    current_step += 1

    print('\nStep {}/{}:'.format(current_step, steps))
    print('Committing changes and shifting git tag\n'
          '---------------------------------------')
    if len(files_to_commit) > 0:
        shift_tag(version_str=version, files_to_add=files_to_commit)
    else:
        print('No changes to commit.')
    print('\nStep {}/{}: Completed'.format(current_step, steps))
    current_step += 1

    print('\nStep {}/{}:'.format(current_step, steps))
    print('Cleaning up\n'
          '-----------')
    remove_version_files()
    print('\nStep {}/{}: Completed'.format(current_step, steps))
    print('Everything seems to be set up fine.\n'
          'You can now push commits and tags to origin to schedule a new F-Droid build job.\n'
          'You can also re-run this script to confirm the checks.')
