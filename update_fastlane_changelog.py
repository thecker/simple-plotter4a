"""
This script takes the information defined in CHANGELOG (ReST format) and creates changelog and adds the
current release info to the fastlane folder structure
"""

import os
import re
from pathlib import Path


def get_manifest_data():
    """Extract the versionCode and versionName from AndroidManifest.xml"""
    with open('AndroidManifest.xml', 'r') as file:
        manifest_data = file.read()
        vercode_match = re.search('versionCode=\"[0-9]+\"', manifest_data)
        manifest_versionCode = vercode_match.group().split('\"')[1]
        vername_match = re.search('versionName=\"[0-9]+.[0-9]+.[0-9]+\"', manifest_data)
        manifest_versionName = vername_match.group().split('\"')[1]

    return manifest_versionCode, manifest_versionName, manifest_data


def check_fastlane_changelogs(fastlane_changelogs_dir, versionCode):
    """Checks if the fastlane changelog already exists"""
    if not Path(fastlane_changelogs_dir).exists():
        os.mkdir(fastlane_changelogs_dir)

    for root, dirs, files in os.walk(fastlane_changelogs_dir):
        if '{}.txt'.format(versionCode) in files:
            overwrite = input('fastlane changelog for versionCode {} already exists. '
                              'Do you want to overwrite it?[y/n=default]:')
            if overwrite in ['y', 'Y']:
                print('fastlane changelog for {} will be replaced.'.format(versionCode))
                return False
            else:
                print('User abort. fastlane changelogs not updated.')
                exit(0)
        else:
            print('fastlane changelog for {} does not exist.'.format(versionCode))
            return False


def read_changelog(changelog_file, versionName):
    """Reads the CHANGELOG file"""
    # read CHANGELOG
    with open(changelog_file, 'r') as file:
        lines = file.readlines()

    release_lines = []
    latest_release = False
    use_lines = False
    for line in lines:
        if line.startswith('release'):  # this should be latest release the first time this is encountered
            if not latest_release and line.startswith('release {}'.format(versionName)):
                use_lines = True
            elif not latest_release:
                print('Latest release does not seem to match release version in AndroidManifest.xml!'
                      'Please update the changelog first!')
                exit(1)
            else:   # this is a previous release
                break
        if use_lines:
            release_lines.append(line)
    if len(release_lines) == 0:
        print('Could not identify release info for release {}.'.format(versionName))
        exit(1)
    else:
        return release_lines


def write_release_file(release_lines, versionCode, fastlane_changelogs_dir):
    """Writes a fastlane changelog file"""

    fastlane_file = Path(fastlane_changelogs_dir).joinpath(versionCode + '.txt')

    with open(fastlane_file, 'w') as file:
        file.writelines(release_lines)

    print('fastlane changelog for {} has been saved to {}'.format(versionCode, fastlane_file))


if __name__ == '__main__':
    # dir where fastlane changelogs are stored
    fastlane_changelogs_dir = 'fastlane/metadata/android/en-US/changelogs'

    # filename of changelog file
    changelog_file = 'CHANGELOG'

    # get current versionCode and versionName from AndroidManifest.xml
    print('\nChecking AndroidManifest.xml for versionCode and versionName...')
    versionCode, versionName, _ = get_manifest_data()
    print('versionCode: {}, versionName: {}'.format(versionCode, versionName))

    # check if fastlane changelog file already exists
    print('\nChecking if fastlane changelog file already exists...')
    exists = check_fastlane_changelogs(fastlane_changelogs_dir, versionCode)

    # read release info from CHANGELOG
    print('\nReading release info from CHANGELOG...')
    release_lines = read_changelog(changelog_file, versionName)
    print('{} lines of release info extracted.'.format(len(release_lines)))

    # write fastlane file
    print('\nCreating fastlane changelog file...')
    write_release_file(release_lines, versionCode, fastlane_changelogs_dir)
